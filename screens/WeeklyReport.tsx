import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import Animated, {
  Easing,
  useAnimatedStyle,
  useSharedValue,
  withDelay,
  withTiming,
} from 'react-native-reanimated'
import { SharedElement } from 'react-navigation-shared-element'
import LottieView from 'lottie-react-native'
import { TextAnimator } from '../components/TextAnimator'
import { SafeAreaView } from 'react-native-safe-area-context'

export const WeeklyReport = () => {
  const LottieRef = React.useRef<LottieView>(null)

  const fadeOut = useSharedValue(1)
  const fadeInOut = useSharedValue(0)

  const onFinish = () => {
    setTimeout(() => {
      fadeOut.value = 0
    }, 200)
    setTimeout(() => {
      fadeInOut.value = 1
    }, 500)
    setTimeout(() => {
      fadeInOut.value = 0
      LottieRef.current?.play()
    }, 1900)
  }

  const fadeOutStyle = useAnimatedStyle(() => ({
    transform: [
      {
        translateY: withTiming(fadeOut.value * -25, {
          duration: 300,
          easing: Easing.bezier(0.86, 0, 0.65, 0.64),
        }),
      },
    ],
    opacity: withDelay(
      180,
      withTiming(fadeOut.value, {
        duration: 100,
      }),
    ),
  }))
  const fadeInOutStyle = useAnimatedStyle(() => ({
    transform: [
      {
        translateY: withTiming(fadeInOut.value * -25, {
          duration: 300,
          easing: Easing.bezier(0.2, 0.8, 0.2, 1),
        }),
      },
    ],
    opacity: withTiming(fadeInOut.value, {
      duration: 300,
      easing: Easing.bezier(0.2, 0.8, 0.2, 1),
    }),
  }))

  const hours = new Date().getHours()
  const isMorningTime = hours >= 6 && hours <= 11
  const isAfternoonTime = hours >= 12 && hours <= 17
  const isEveningTime = hours >= 18 && hours <= 24

  const welcomeText = (name = 'Frans') => {
    if (isMorningTime) return `Good morning, ${name}!`
    if (isAfternoonTime) return `Good afternoon, ${name}!`
    if (isEveningTime) return `Good evening, ${name}!`
    return `What's up, ${name}!`
  }

  return (
    <SafeAreaView style={styles.container}>
      <SharedElement style={[StyleSheet.absoluteFill]} id="bg">
        <View
          style={{
            width: '100%',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <LottieView
            resizeMode="cover"
            ref={LottieRef}
            loop={false}
            style={[{ position: 'absolute', width: '100%', height: '100%' }]}
            source={require('../assets/animations/timerange.json')}
          />
          <Animated.View style={[fadeOutStyle]}>
            <TextAnimator
              textStyle={styles.textStyle}
              duration={200}
              delay={800}
              text={welcomeText()}
              onFinish={onFinish}
            />
          </Animated.View>
          <Animated.View style={[{ marginTop: -35 }, fadeInOutStyle]}>
            <Text style={[styles.textStyle]}>Let's review your week.</Text>
          </Animated.View>
        </View>
      </SharedElement>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',

    justifyContent: 'center',
    paddingTop: 57,
  },
  textStyle: {
    fontFamily: 'Graphik-Medium',
    fontWeight: 'bold',
    fontSize: 24,
  },
})
