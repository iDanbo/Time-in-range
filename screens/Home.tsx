import React from 'react'
import { StackScreenProps } from '@react-navigation/stack'
import { Pressable, View, StyleSheet, Text } from 'react-native'
import Animated, {
  Easing,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated'
import { SharedElement } from 'react-navigation-shared-element'
import { RootStackParamList } from '../App'
import { StatusBar } from 'expo-status-bar'

export const Home = ({ navigation }: StackScreenProps<RootStackParamList, 'Home'>) => {
  const scale = useSharedValue(1)
  const SCALE_OFFSET = 0.1

  const onPressIn = useAnimatedStyle(() => {
    return {
      transform: [
        {
          scale: withTiming(scale.value, {
            duration: 500,
            easing: Easing.bezier(0.2, 0.8, 0.2, 1),
          }),
        },
      ],
    }
  })

  return (
    <View style={styles.container}>
      <Animated.View style={[{ width: '80%', height: '25%' }, onPressIn]}>
        <Pressable
          style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center' }}
          onPressIn={() => {
            scale.value -= SCALE_OFFSET
          }}
          onPressOut={() => {
            scale.value += SCALE_OFFSET
            navigation.navigate('WeeklyReport')
          }}
        >
          <SharedElement style={[StyleSheet.absoluteFill]} id="bg">
            <View
              style={[
                { borderRadius: 18 },
                StyleSheet.absoluteFill,
                {
                  shadowColor: '#000',
                  shadowOffset: {
                    width: 0,
                    height: 2,
                  },
                  shadowOpacity: 0.15,
                  shadowRadius: 8.68,

                  // elevation: 11,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'rgba(63,108,83,1)',
                },
              ]}
            ></View>
          </SharedElement>
          <SharedElement id="text">
            <Text
              style={[
                {
                  fontFamily: 'Graphik-Medium',
                  fontWeight: 'bold',
                  fontSize: 24,
                  color: 'rgba(228,241,234,1)',
                },
              ]}
            >
              Weekly Report
            </Text>
          </SharedElement>
        </Pressable>
      </Animated.View>
      <StatusBar style="auto" />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
})
