import React from 'react'
import { View, StyleSheet, Animated, ViewStyle, TextStyle } from 'react-native'

// Source code: https://github.com/catalinmiron/react-native-animated-sentence/blob/master/components/TextAnimator.js
// modified it a bit

type TextAnimatorProps = {
  text: string
  delay?: number
  duration?: number
  style?: ViewStyle
  textStyle?: TextStyle
  onFinish?: () => void
}

export const TextAnimator: React.FC<TextAnimatorProps> = (props) => {
  const animatedValues: any = []

  const textArr = props.text.trim().split('')

  textArr.forEach((_, i) => {
    animatedValues[i] = new Animated.Value(0)
  })

  React.useEffect(() => {
    setTimeout(() => animated(), props.delay || 0)
  }, [])

  const animated = (toValue = 1) => {
    const animations = textArr.map((_, i) => {
      return Animated.spring(animatedValues[i], {
        toValue,
        velocity: 30,
        useNativeDriver: true,
      })
    })

    Animated.stagger(props.duration / 5, animations).start(() => {
      if (props.onFinish) {
        props.onFinish()
      }
    })
  }

  return (
    <View style={[props.style, styles.textWrapper]}>
      {textArr.map((word: string, index: number) => {
        return (
          <Animated.Text
            key={`${word}-${index}`}
            style={[
              props.textStyle,
              {
                opacity: animatedValues[index],
                transform: [
                  {
                    translateY: Animated.multiply(animatedValues[index], new Animated.Value(-15)),
                  },
                  {
                    rotateZ: animatedValues[index].interpolate({
                      inputRange: [0, 1],
                      outputRange: ['15deg', '0deg'],
                    }),
                  },
                ],
              },
            ]}
          >
            {word}
          </Animated.Text>
        )
      })}
    </View>
  )
}

const styles = StyleSheet.create({
  textWrapper: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
})
