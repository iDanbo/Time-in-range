import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { useFonts } from 'expo-font'
import { createSharedElementStackNavigator } from 'react-navigation-shared-element'
import { MaterialIcons } from '@expo/vector-icons'
import { Home } from './screens/Home'
import { WeeklyReport } from './screens/WeeklyReport'

export type RootStackParamList = {
  Home: undefined
  WeeklyReport: undefined
}

// @ts-ignore
const forFade = ({ current }) => ({
  cardStyle: {
    opacity: current.progress,
  },
})

export default function App() {
  let [fontsLoaded] = useFonts({
    'Graphik-Medium': require('./assets/fonts/Graphik-Medium.otf'),
  })

  const Stack = createSharedElementStackNavigator<RootStackParamList>()

  if (!fontsLoaded) {
    return null
  }
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: true,
          headerTransparent: true,
          headerBackTitleVisible: false,
          headerTitle: '',
          headerStyle: { shadowColor: 'transparent', elevation: 0 },
          cardStyleInterpolator: forFade,
        }}
      >
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen
          name="WeeklyReport"
          component={WeeklyReport}
          sharedElements={() => {
            return [
              { id: 'bg', animation: 'fade' },
              { id: 'text', animation: 'fade' },
            ]
          }}
          options={{
            headerStyle: {
              backgroundColor: 'transparent',
              shadowColor: 'transparent',
              elevation: 0,
            },

            headerBackImage: () => (
              <MaterialIcons style={{ marginLeft: 12 }} size={30} name="arrow-back" />
            ),
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  )
}
